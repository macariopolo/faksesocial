package edu.uclm.esi.fakeSocial.http;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import edu.uclm.esi.fakeSocial.dao.PostRepository;
import edu.uclm.esi.fakeSocial.dao.UserRepository;
import edu.uclm.esi.fakeSocial.model.Post;
import edu.uclm.esi.fakeSocial.model.User;
import edu.uclm.esi.fakeSocial.services.PostService;
import edu.uclm.esi.fakeSocial.wrappers.WrapperPost;

@RestController
@RequestMapping("posts")
public class PostController extends CookiesController {
	
	@Autowired
	private PostRepository postRepo;
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired PostService postService;
	
	@GetMapping("/get")
	public List<WrapperPost> get() {
		List<Post> posts = postRepo.findAll();
		posts.removeIf(post -> post.getParentPost()!=null);
		posts.sort(new Comparator<Post>() {

			@Override
			public int compare(Post a, Post b) {
				return (a.getDate()<b.getDate()) ? 1 : -1;
			}
		});
		return WrapperPost.convert(posts);
	}
	
	@GetMapping("/get/{postId}")
	public WrapperPost get(@PathVariable String postId) {
		Optional<Post> optPost = postRepo.findById(postId);
		if (!optPost.isPresent())
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No se encuentra el post");
		Post post = optPost.get();
		WrapperPost result = new WrapperPost(post);
		return result;
	}
	
	@PostMapping("/newPost")
	public void newPost(HttpSession session, @RequestBody Map<String, String> info) {
		if (session.getAttribute("userId")==null)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Tienes que estar logueado para poner posts");
		User user = userRepo.findById(session.getAttribute("userId").toString()).get();
		try {
			postService.savePost(user, info);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}		
	}
	
	@PutMapping("/addComment/{parentPostId}")
	public void addComment(HttpSession session, @PathVariable String parentPostId, @RequestBody Map<String, Object> info) {
		if (session.getAttribute("userId")==null)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Tienes que estar logueado para poner posts");
		User user = userRepo.findById(session.getAttribute("userId").toString()).get();
		try {
			postService.addComment(user, parentPostId, info);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}		
	}
	
	@DeleteMapping("/delete/{postId}")
	public void delete(HttpSession session, @PathVariable String postId) {
		if (session.getAttribute("userId")==null)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Tienes que estar logueado para poner posts");
		User user = userRepo.findById(session.getAttribute("userId").toString()).get();
		try {
			postService.delete(user, postId);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}
}
