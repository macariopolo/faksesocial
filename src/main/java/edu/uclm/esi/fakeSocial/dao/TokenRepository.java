package edu.uclm.esi.fakeSocial.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.uclm.esi.fakeSocial.model.Token;

public interface TokenRepository extends JpaRepository<Token, String> {

}
