package edu.uclm.esi.fakeSocial.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import edu.uclm.esi.fakeSocial.model.Post;

@Repository
public interface PostRepository extends JpaRepository <Post, String> {

	@Query(value = "select count(*) from post where parent_post_id= :parentPostId", nativeQuery = true)
	Integer getNumberOfComments(@Param("parentPostId") String parentPostId);

	@Query(value = "select * from post where parent_post_id= :parentPostId", nativeQuery = true)
	List<Post> loadComments(@Param("parentPostId") String parentPostId);

}
