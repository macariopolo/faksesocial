package edu.uclm.esi.fakeSocial.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.uclm.esi.fakeSocial.model.Login;

public interface LoginRepository extends JpaRepository <Login, String> {

}
