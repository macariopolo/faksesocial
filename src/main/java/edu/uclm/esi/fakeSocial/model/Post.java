package edu.uclm.esi.fakeSocial.model;

import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Post {
	@Id
	@Column(length = 36)
	private String id;
	
	private String text;
	private long date;
	
	@ManyToOne
	private Post parentPost;
	
	@ManyToOne(cascade = CascadeType.ALL)
	private User author;
	
	public Post() {
		this.id = UUID.randomUUID().toString();
		this.date = System.currentTimeMillis();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}
	
	public void setParentPost(Post parentPost) {
		this.parentPost = parentPost;
	}
	
	public Post getParentPost() {
		return parentPost;
	}
	
	public long getDate() {
		return date;
	}
	
	public void setDate(long date) {
		this.date = date;
	}
}
