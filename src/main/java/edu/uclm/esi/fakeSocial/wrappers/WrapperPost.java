package edu.uclm.esi.fakeSocial.wrappers;

import java.util.ArrayList;
import java.util.List;

import edu.uclm.esi.fakeSocial.http.Manager;
import edu.uclm.esi.fakeSocial.model.Post;

public class WrapperPost {
	private String id;
	private String text;
	private String author;
	private List<WrapperPost> subposts;

	public WrapperPost(Post post) {
		this.id = post.getId();
		this.text = post.getText();
		this.author = Manager.get().getUsersRepository().findById(post.getAuthor().getId()).get().getName();
		this.subposts = new ArrayList<>();
		List<Post> subposts = Manager.get().getPostsRepository().loadComments(this.id);
		for (Post subpost : subposts)
			this.subposts.add(new WrapperPost(subpost));
	}

	public static List<WrapperPost> convert(List<Post> posts) {
		ArrayList<WrapperPost> result = new ArrayList<>();
		for (Post post : posts) {
			result.add(new WrapperPost(post));
		}
		return result;
	}

	public String getText() {
		return text;
	}

	public String getAuthor() {
		return author;
	}
	
	public Integer getComments() {
		return this.subposts.size();
	}

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	public void addSubposts(List<Post> subposts) {
		for (Post subpost : subposts) {
			WrapperPost wp = new WrapperPost(subpost);
			this.subposts.add(wp);
		}
	}
	
	public List<WrapperPost> getSubposts() {
		return subposts;
	}
}
