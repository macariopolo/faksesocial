package edu.uclm.esi.fakeSocial.services;

import java.util.Map;
import java.util.Optional;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import edu.uclm.esi.fakeSocial.dao.PostRepository;
import edu.uclm.esi.fakeSocial.model.Post;
import edu.uclm.esi.fakeSocial.model.User;

@Service
public class PostService {
	
	@Autowired
	private PostRepository postRepo;

	public void savePost(User author, Map<String, String> info) throws Exception {
		JSONObject jso = new JSONObject(info);
		if (jso.optString("text").trim().length()==0)
			throw new Exception("El texto de tu post no puede estar vacío");
		Post post = new Post();
		post.setAuthor(author);
		post.setText(jso.getString("text"));
		postRepo.save(post);
	}

	public void addComment(User author, String parentPostId, Map<String, Object> info) throws Exception {
		Optional<Post> parentPost = postRepo.findById(parentPostId);
		if (!parentPost.isPresent())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No se encuentra el post que quieres comentar");
		
		JSONObject jso = new JSONObject(info);
		if (jso.optString("commentText").trim().length()==0)
			throw new Exception("El texto de tu comentario no puede estar vacío");
		Post post = new Post();
		post.setAuthor(author);
		post.setText(jso.getString("commentText"));
		post.setParentPost(parentPost.get());
		postRepo.save(post);
	}

	public void delete(User user, String postId) {
		Optional<Post> optPost = postRepo.findById(postId);
		if (!optPost.isPresent())
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No se encuentra el post que quieres eliminar");
		Post post = optPost.get();
		if (!post.getAuthor().getId().equals(user.getId()))
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Solo puedes eliminar los posts que has creado");
		postRepo.delete(post);
	}
	
}
