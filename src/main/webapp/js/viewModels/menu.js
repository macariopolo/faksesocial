define([ 'knockout', 'appController', 'ojs/ojmodule-element-utils', 'accUtils',
		'jquery' ], function(ko, app, moduleUtils, accUtils, $) {

	class MenuViewModel {
		constructor() {
			var self = this;

			self.userName = ko.observable(null);
			self.error = ko.observable(null);

			self.postText = ko.observable(null);

			self.posts = ko.observableArray([]);
						
			// Header Config
			self.headerConfig = ko.observable({
				'view' : [],
				'viewModel' : null
			});
			moduleUtils.createView({
				'viewPath' : 'views/header.html'
			}).then(function(view) {
				self.headerConfig({
					'view' : view,
					'viewModel' : app.getHeaderModel()
				})
			})
		}

		connected() {
			accUtils.announce('Menu page loaded.');
			document.title = "Menu";

			let self = this;
			let data = {
				url : "/user/getUserName",
				type : "get",
				success : function(response) {
					self.userName(response);
					self.error(null);
					self.getPosts();
				},
				error : function(response) {
					self.error(response);
					self.userName(null);
				} 
			}
			$.ajax(data);
		};

		getPosts() {
			let self = this;
			let data = {
				url : "/posts/get",
				type : "get",
				success : function(response) {
					self.posts([]);
					for (let i=0; i<response.length; i++) {
						let post = response[i];
						post.subposts = ko.observableArray([]);
						post.commentEnabled = ko.observable(false);
						post.commentText = ko.observable(null);
						self.posts.push(post);
					}
				}
			}
			$.ajax(data);
		}

		newPost() {
			let self = this;
			let info = {
				text : this.postText()
			};
			let data = {
				url : "posts/newPost",
				data : JSON.stringify(info),
				type : "post",
				contentType : "application/json",
				success : function() {
					self.getPosts();
				}
			}
			$.ajax(data);
		}

		sendComment(post) {
			let self = this;
			let info = {
				commentText : post.commentText()
			};
			let data = {
				url : "posts/addComment/" + post.id,
				data : JSON.stringify(info),
				type : "put",
				contentType : "application/json",
				success : function() {
					self.getPosts();
				}
			}
			$.ajax(data);
		}

		showComments(post) {
			let self = this;
			let data = {
				url : "posts/get/" + post.id,
				type : "get",
				contentType : "application/json",
				success : function(response) {
					post.subposts(response.subposts);
				}
			}
			$.ajax(data);
		}

		enableComments(post) {
			post.commentEnabled(true);
		}

		deletePost(post) {
			let self = this;
			let data = {
				url : "posts/delete/" + post.id,
				type : "delete",
				success : function(response) {
					post.subposts(response.subposts);
					self.getPosts();
				},
				error : function(response) {
					self.error(response.responseJSON.message);
				}
			}
			$.ajax(data);
		}

		disconnected() {
			// Implement if needed
		};

		transitionCompleted() {
			// Implement if needed
		};
	}

	return MenuViewModel;
});
